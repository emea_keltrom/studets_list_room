package com.example.myapplication.callback;

import com.example.myapplication.model.Students;

public interface ItemCLickCallBack {
    void itemClick(Students students);
}
