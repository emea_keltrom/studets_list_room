package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.callback.ItemCLickCallBack;
import com.example.myapplication.R;
import com.example.myapplication.model.Students;

import java.util.List;

public class StudentsListAdapter extends RecyclerView.Adapter<StudentsListAdapter.MyViewHolder> {
    List<Students> studentsList;
    ItemCLickCallBack callBack;

    public StudentsListAdapter(List<Students> studentsList,ItemCLickCallBack callBack) {
        this.studentsList = studentsList;
        this.callBack =callBack;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.students_list_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Students studentsData= studentsList.get(position);
        holder.txtName.setText(studentsData.getName());
        holder.txtEmail.setText(studentsData.getEmail());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.itemClick(studentsData);
            }
        });



    }

    @Override
    public int getItemCount() {
        return studentsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtEmail;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.textName);
            txtEmail = itemView.findViewById(R.id.textEmail);
        }
    }
}
