package com.example.myapplication.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.myapplication.R;

public class AddStudentActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    EditText txtName, txtEmail, txtMobile;
    Spinner spnrDepartment;
    Button btSave;

    String[] departmentList = {"CS", "EC", "EEE", "CA", "CIVIL"};

    String selectedDepartment;
    String department = "CS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        txtName = findViewById(R.id.textStudentName);
        txtEmail = findViewById(R.id.textStudentEmail);
        txtMobile = findViewById(R.id.textStudentMobile);
        spnrDepartment = findViewById(R.id.spinnerDepartment);
        btSave = findViewById(R.id.btnSave);

        spnrDepartment.setOnItemSelectedListener(this);

        ArrayAdapter spinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, departmentList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnrDepartment.setAdapter(spinnerAdapter);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = txtName.getText().toString();
                String email = txtEmail.getText().toString();
                String mobile = txtMobile.getText().toString();

            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        department = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}