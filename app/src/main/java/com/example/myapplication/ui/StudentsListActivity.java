package com.example.myapplication.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.myapplication.callback.ItemCLickCallBack;
import com.example.myapplication.R;
import com.example.myapplication.adapter.StudentsListAdapter;
import com.example.myapplication.model.Students;

import java.util.ArrayList;
import java.util.List;

public class StudentsListActivity extends AppCompatActivity implements ItemCLickCallBack {

    RecyclerView rvStudents;
    List<Students> studentsList;
    StudentsListAdapter studentsListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list);

        rvStudents = findViewById(R.id.rvStudentsList);
        studentsList=new ArrayList<>();
        setStudentsList();
        studentsListAdapter = new StudentsListAdapter(studentsList,this);
        rvStudents.setLayoutManager(new LinearLayoutManager(this));
        rvStudents.setAdapter(studentsListAdapter);
    }


    void setStudentsList(){
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
        studentsList.add(new Students("Amal","amal@email.com","0808080","CS"));
    }

    @Override
    public void itemClick(Students students) {

    }
}