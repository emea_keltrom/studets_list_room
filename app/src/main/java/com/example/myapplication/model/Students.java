package com.example.myapplication.model;

public class Students {
    String name;
    String email;
    String mobile;
    String department;

    public Students(String name, String email, String mobile, String department) {
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
